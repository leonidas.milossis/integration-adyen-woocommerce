const gulp    = require('gulp');
const path    = require('path');
const replace = require('gulp-string-replace');
const rename  = require('gulp-rename');
const rimraf  = require('gulp-rimraf');
const del     = require('del');
const exec    = require('child_process').exec;
const CONFIG  = require('./config.json');

const currentFolderPath = path.resolve(__dirname, './');
const currentFolderName = path.basename(path.dirname(currentFolderPath+'/gulpfile.js'));


/*
|--------------------------------------------------------------------------
| Build the base
|--------------------------------------------------------------------------
*/

gulp.task('install', function (cb) {
   return exec('rm -rf vendor && rm -rf composer.lock && composer install', function (err, stdout, stderr) {
      console.log(stdout);
      console.log(stderr);
      cb(err);
   });
});

gulp.task('rename_files', function () {
   return gulp.src([
      './wp-plugin-starter.*',
      './**/*/wp-plugin-starter.*',
   ])
   .pipe(rimraf())
   .pipe(rename(function(path) {
      path.basename = currentFolderName;
   }))
   .pipe(gulp.dest(currentFolderPath));
});

gulp.task('replace_strings', function() {
   return gulp.src([
      '**/*',
      '!**/*.md',
      '!**/*.png',
      '!**/*.jpeg',
      '!**/*.jpg',
      '!**/*.gif',
      '!**/*.webp',
      '!**/*.po',
      '!**/*.pot',
      '!**/*.mo',
      '!config.js',
      '!gulpfile.js',
      '!dist/',
   ], {
      base: currentFolderPath
   })
   .pipe(replace('_wsa_namespace_', CONFIG.namespace))
   .pipe(replace('_wsa_plugin_name_', CONFIG.plugin.name))
   .pipe(replace('_wsa_plugin_desc_', CONFIG.plugin.description))
   .pipe(replace('_wsa_plugin_uri_', CONFIG.plugin.uri))
   .pipe(replace('_wsa_prefix_', CONFIG.plugin.prefix))
   .pipe(replace('_wsa_js_prefix_', CONFIG.plugin.prefix))
   .pipe(replace('_wsa_css_prefix_', CONFIG.plugin.prefix))
   .pipe(replace('_wsa_text_domain_', CONFIG.plugin.text_domain))
   .pipe(replace('_wsa_settings_tab_id_', CONFIG.plugin.settings_tab_id))
   .pipe(replace('_wsa_settings_tab_name_', CONFIG.plugin.settings_tab_name))
   .pipe(gulp.dest(currentFolderPath))
});



/*
|--------------------------------------------------------------------------
| Build DIST folder
|--------------------------------------------------------------------------
*/

//delete dist folder
gulp.task('remove_dist', function () {
   return del([
      'dist'
   ]);
});

//remove dev vendor
gulp.task('vendor_for_prod', function (cb) {
   exec('composer install --no-dev && rm -rf vendor/bin', function (err, stdout, stderr) {
      console.log(stdout);
      console.log(stderr);
      cb(err);
   });
});

//copy files to dist folder
gulp.task('pack_dist_files', function () {
   return gulp.src([
      'assets/**/*',
      'includes/**/*',
      'languages/**/*',
      'templates/**/*',
      'vendor/**/*',
      'readme.txt',
      '*.php',
   ], {
      base: currentFolderPath
   })
   .pipe(gulp.dest('dist/'+currentFolderName));
});




/*
|--------------------------------------------------------------------------
| MAIN TASKS
|--------------------------------------------------------------------------
*/

//try to run local tasks if any
try{

   const { localTasks } = require('./gulp-local-tasks');
   gulp.task('localTasks', localTasks);

} catch (error) {
   console.log(error);
}

gulp.task('default', gulp.series(
   'install',
   'rename_files',
   'replace_strings',
));

gulp.task('build', gulp.series(
   'replace_strings',
   'vendor_for_prod',
   'remove_dist',
   'pack_dist_files',
));